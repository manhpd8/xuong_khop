<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

$res = null;
if (isset($_SERVER['QUERY_STRING'])) {
	$file = 'fb.txt';
	$content = PHP_EOL. (new \DateTime())->format('Y-m-d H:i:s'). ' '. $_SERVER['QUERY_STRING'];
	file_put_contents($file, $content, FILE_APPEND | LOCK_EX);
	$res = [
		'status' => 200,
		'data' => $content
	];
	echo json_encode($res);
}
