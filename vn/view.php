<?php

$filename = 'on_off.txt';
$onOff = false;
if ($file = fopen($filename, "r")) {
        $line = fgets($file);
	if (trim($line) == '1') {
		$onOff = true;
	}
	fclose($file);
}

if (isset($_POST['switch'])) {
	$newValue = ($onOff ? '0' : '1');
	file_put_contents($filename, $newValue);
	$onOff = ($onOff ? false: true);
	header("Refresh:0");
}

echo 'STATUS: '. ($onOff ? 'ON' : 'OFF') .' ';
echo '<form method="post" action="./view.php"><input type="hidden" name="switch"/><input type="submit" value="SWITCH"/></form>';

$filename = 'fb.txt';
if ($file = fopen($filename, "r")) {
    while(!feof($file)) {
        $line = fgets($file);
        echo $line .'<br>';
    }
    fclose($file);
}
