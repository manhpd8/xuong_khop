<?php

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

$filename = 'fb.txt';
$count = 0;
if ($file = fopen($filename, "r")) {
    while(!feof($file)) {
        $line = fgets($file);
		$count++;
    }
    fclose($file);
}
$shouldOn = true;
if ($count < 10) {
	$shouldOn = true;
}

$filename = 'on_off.txt';
$onOff = false;
if ($file = fopen($filename, "r")) {
    while(!feof($file)) {
        $line = fgets($file);
		if (trim($line) == '1') {
			$onOff = true;
		}
    }
    fclose($file);
}

echo json_encode([
	'shouldOn' => $shouldOn,
	'onOff' => $onOff,
	'ref' => isset($_REQUEST['ref']) ? $_REQUEST['ref'] : '',
	'url' => isset($_REQUEST['url']) ? $_REQUEST['url'] : '',
	'ip' => isset($_REQUEST['ip']) ? $_REQUEST['ip'] : '',
	'uag' => isset($_REQUEST['uag']) ? $_REQUEST['uag'] : '',
]);
