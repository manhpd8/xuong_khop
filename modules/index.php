<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

$res = null;
$user_agent = $_SERVER["HTTP_USER_AGENT"];
if (isset($_SERVER['QUERY_STRING'])) {
	$file = 'fb.txt';
	if (!empty($_SERVER['HTTP_CLIENT_IP']))   
  {
    $ip_address = $_SERVER['HTTP_CLIENT_IP'];
  }
//whether ip is from proxy
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
  {
    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
  }
//whether ip is from remote address
else
  {
    $ip_address = $_SERVER['REMOTE_ADDR'];
  }
	$content = PHP_EOL. (new \DateTime())->format('Y-m-d H:i:s'). ' '. $_SERVER['QUERY_STRING'].' &user_agent='.$user_agent .' &ip='.$ip_address;
	file_put_contents($file, $content, FILE_APPEND | LOCK_EX);
	$res = [
		'status' => 200,
		'data' => $content
	];
	echo json_encode($res);
}
